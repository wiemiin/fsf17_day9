### HOW TO COMMIT CODES ###

* git add . 
* git commit -m "update" 
* git push origin master

### SET UP ###

* git init 
* git remote add origin <git URL>
* mkdir server directory > app.js 
* mkdir client directory > app > app.module.js app.controller.js css > main.css
* create .gitignore (add node_modules and client/bower_components) 
* npm init (create package.json, enter entry point server/app.js)
* npm install express body-parser - - save  (root) 
* bower init (create package.json, enter entry point server/app.js)
* create .bowerrc (directory: "client/bower_components") 
* bower install bootstrap font-awesome angular - - save


### START SERVER ###

* go back to day4
* export PORT=4001
* nodemon

### AFTER UPLOAD TO GIT, CLONE AGAIN TO CHECK ###

* git clone
* npm install
* bower install 
