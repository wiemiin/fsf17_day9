
(function() {
    
    "use strict";
    angular.module("RegApp")
        .controller("FilterCtrl", FilterCtrl); 

        FilterCtrl.$inject = ['$filter']; 

        function FilterCtrl() {
            var self = this;
            self.search = "";
            self.propertyName = "id";
            var today = new Date();
            self.reverse = false; //reverse the order sorting by setting false

            self.items = [
                { id: 1, name: "A", age: 20, dob: today },
                { id: 2, name: "B", age: 21, dob: today },
                { id: 8, name: "H", age: 20, dob: today },
                { id: 3, name: "C", age: 20, dob: today },
                { id: 4, name: "D", age: 20, dob: today },
                { id: 5, name: "E", age: 20, dob: today },
                { id: 6, name: "F", age: 20, dob: today },
                { id: 7, name: "G", age: 18, dob: "Dec" },
                { id: 9, name: "I", age: 20, dob: today },
                { id: 10, name: "A", age: 20, dob: "Nov" }
            ]

            self.sortBy = function(propertyName) {
                    console.log(!self.reverse);
                    console.log(self.propertyName === propertyName);
                    self.reverse = self.reverse = (self.propertyName === propertyName) ? !self.reverse : false; //if statement ? else : 
                    console.log(self.reverse);
                    self.propertyName === propertyName;
                    
                   // if (self.propertyName === propertyName) {
                   //     self.reverse = !self.reverse;
                   // } else {
                   //     self.reverse = false; 
                   // }

                    

            }
        }

})();
