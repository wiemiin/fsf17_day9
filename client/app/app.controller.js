/**
 * Client side code.
 */

(function() {
    
    "use strict";
    angular.module("RegApp")
        .controller("RegCtrl", RegCtrl)
        //.controller("RegisteredCtrl", RegisteredCtrl);
   
    RegCtrl.$inject = ['RegService', "$log"]; //custom service 'single quote', angular $ service must be "double quote" 
    //RegisteredCtrl.$inject = ['RegService', "$log"];
    
    //function RegisteredCtrl(RegService, $log) {
    //    var self = this;		
    //    console.log(">>> " + RegService.getFullname());		
    //    self.fullname = RegService.getFullname();
    //};

    function RegCtrl(RegService, $log) { //remove $http, add RegService
        var regCtrlself = this; 

        regCtrlself.onSubmit = onSubmit;
        regCtrlself.initForm = initForm;
        regCtrlself.onlyFemale = onlyFemale;
        regCtrlself.reset = reset; 
        //regCtrlself.isSubmitted = false; 

        regCtrlself.emailFormat = /^[a-z]+[a-z0-9._]+@[a-z]+\.[a-z.]{2,5}$/;

        regCtrlself.user = {

        };

        regCtrlself.nationalities = [
            { name: "Please select", value: 0},
            { name: "Singaporean", value: 1},
            { name: "Indonesian", value: 2},
            { name: "Thai", value: 3},
            { name: "Malaysian", value: 4},
            { name: "Vietnamese", value: 5}
        ];

        function initForm() {
            regCtrlself.user.selectedNationality = "0";
            regCtrlself.user.gender = "F";
            regCtrlself.isSubmitted = true; 
        };

        function reset() {
            regCtrlself.user = Object.assign({}, regCtrlself.user);	
            regCtrlself.registrationform.$setPristine();
            regCtrlself.registrationform.$setUntouched();
          };

        function onSubmit() {
            console.log(RegService); //use RegService here 
            RegService.register(regCtrlself.user) 
            .then(function(result) {
                regCtrlself.user = result;
                console.log($log);
                $log.info(result);
                regCtrlself.isSubmitted = true; 
            }).catch(function(e){
                console.log(e);
            })
        };

        //custom validation
        function onlyFemale() {
            console.log("Only female");
            return regCtrlself.user.gender == "F";
        };
       
          regCtrlself.initForm();

    }

})();






/**
 * Starter code 

(function () {
    
    "use strict";
    angular.module("RegApp").controller("RegCtrl", RegCtrl);

    RegCtrl.$inject = ["$http"];

    function RegCtrl($http) {

    }

})();

 */